<?php
namespace classes;

use \classes\InterfaceSmsConfirmation;

/**
 * Class YandexMoneySmsParser
 *
 * @package classes
 */
class YandexMoneySmsParser
{
    /**
     * @var \classes\InterfaceSmsConfirmation
     */
    protected $yandexMoneySmsConfirmation;

    /**
     * YandexMoneySmsParser constructor.
     *
     * @param \classes\InterfaceSmsConfirmation $yandexMoneySms
     */
    public function __construct(InterfaceSmsConfirmation $yandexMoneySms)
    {
        $this->yandexMoneySmsConfirmation = $yandexMoneySms;
    }

    /**
     * Метод парсинга СМС сообщения
     *
     * @param string $sms - СМС сообщение
     *
     * @return \classes\InterfaceSmsConfirmation
     */
    public function parse(string $sms): InterfaceSmsConfirmation
    {
        $patternSms = '/(?<walletNumber>\d{11,20})|(?<amountCurrency>[\d,]+\D+\.)|(?<code>\d+)/';
        preg_match_all($patternSms, $sms, $matchesSms);
        $walletNumber   = $this->getValueMatches($matchesSms['walletNumber']);
        $code           = $this->getValueMatches($matchesSms['code']);
        $amountCurrency = $this->getValueMatches($matchesSms['amountCurrency']);
        $amount         = '';
        if (!empty($amountCurrency)) {
            $patternAmountCurrency = '/(?<amount>[\d,]+)|(?<currency>.*\.)/';
            preg_match_all($patternAmountCurrency, $amountCurrency, $matches);
            $amount   = str_replace(',', '.', $this->getValueMatches($matches['amount']));
        }
        if (empty($walletNumber) || empty($amount) || empty($code)) {
            throw new \InvalidArgumentException('Не удалось найти один из параметров в СМС');
        }
        $this->yandexMoneySmsConfirmation->setWalletNumber($walletNumber);
        $this->yandexMoneySmsConfirmation->setCode($code);
        $this->yandexMoneySmsConfirmation->setAmount($amount);

        return $this->yandexMoneySmsConfirmation;
    }

    /**
     * Метод получения значения из массива совпавших значений
     *
     * @param array $matches - массив совпавших значений,
     *                       полученный при разборе регулярным выражением
     *
     * @return mixed
     */
    protected function getValueMatches(array $matches)
    {
        $filterMatches = array_filter($matches);

        return reset($filterMatches);
    }
}