<?php
namespace classes;

use \classes\InterfaceSmsConfirmation;

/**
 * Class YandexMoneySmsConfirmation
 *
 * @package classes
 */
class YandexMoneySmsConfirmation implements InterfaceSmsConfirmation
{
    /**
     * @var string - номер кошелька
     */
    protected $walletNumber;
    /**
     * @var string - код подтверждения
     */
    protected $code;
    /**
     * @var float - сумма
     */
    protected $amount;

    /**
     * @return string
     */
    public function getWalletNumber(): string
    {
        return $this->walletNumber;
    }

    /**
     * @param string $walletNumber
     */
    public function setWalletNumber(string $walletNumber): void
    {
        $this->walletNumber = $walletNumber;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }
}