<?php
namespace classes;

/**
 * Interface InterfaceSmsConfirmation
 */
interface InterfaceSmsConfirmation
{
    /**
     * @return string
     */
    public function getWalletNumber(): string;

    /**
     * @param string $walletNumber
     */
    public function setWalletNumber(string $walletNumber): void;

    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @param string $code
     */
    public function setCode(string $code): void;

    /**
     * @return float
     */
    public function getAmount(): float;

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void;
}