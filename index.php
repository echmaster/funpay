<?php
require __DIR__.'/vendor/autoload.php';
$parser  = new classes\YandexMoneySmsParser(new \classes\YandexMoneySmsConfirmation());
$listSms = [
    'Пароль: 0585 Спишется 3232,17руб. Перевод на счет 4100175017397',
    'Пароль: 8715 Спишется 1507,54 р. Перевод на счет 410011435701025',
    'Пароль: 4594<br>
Спишется 366,84р.<br>
Перевод на счет 4100175017397',
    'Никому не говорите пароль! Его спрашивают только мошенники. Пароль: 74777 Перевод на счет 4100175017397 Вы потратите 8040,21р.',
    'Спишется 0,51р. Перевод на счет 4100175017397 Пароль: 7702',
    'Пароль:4425Спишется100,51р.Переводнасчет4100175017397',
    'Недостаточно средств.',
    'Сумма указана неверно.',
];
foreach ($listSms as $item) {
    try {
        echo '<strong>Текст сообщения:</strong><br>';
        echo $item.'<br>';
        $result = $parser->parse($item);
        echo '<strong>Извлеченные данные:</strong><br>';
        echo 'номер кошелька: '.$result->getWalletNumber().'<br>';
        echo 'код: '.$result->getCode().'<br>';
        echo 'сумма: '.$result->getAmount();
    } catch (\Throwable $e) {
        echo '<strong>Ошибка:</strong><br>';
        echo $e->getMessage();
    }
    echo '<br>-------------------------<br>';
}